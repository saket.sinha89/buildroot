################################################################################
#
# python-udsoncan
#
################################################################################

PYTHON_UDSONCAN_VERSION = 1.23.0
PYTHON_UDSONCAN_SOURCE = udsoncan-$(PYTHON_UDSONCAN_VERSION).tar.gz
PYTHON_UDSONCAN_SITE = https://files.pythonhosted.org/packages/e9/4e/8bb11738ba84ddb588925b523afbc981f9db5449426dd188c1be9c8beb5f
PYTHON_UDSONCAN_LICENSE = MIT
PYTHON_UDSONCAN_LICENSE_FILES = LICENSE.txt
PYTHON_UDSONCAN_CPE_ID_VENDOR = udsoncan_project
PYTHON_UDSONCAN_CPE_ID_PRODUCT = udsoncan
PYTHON_UDSONCAN_SETUP_TYPE = setuptools

$(eval $(python-package))

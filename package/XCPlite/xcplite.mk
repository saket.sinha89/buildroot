################################################################################
#
# xcplite
#
################################################################################

XCPLITE_VERSION = 6.2
XCPLITE_SITE = $(call github,disdi,XCPlite,$(XCPLITE_VERSION))
XCPLITE_INSTALL_STAGING = NO
XCPLITE_INSTALL_TARGET = YES
XCPLITE_LICENSE = MIT
XCPLITE_LICENSE_FILES = LICENSE

$(eval $(cmake-package))

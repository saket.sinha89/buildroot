################################################################################
#
# python-can-isotp
#
################################################################################

PYTHON_CAN_ISOTP_VERSION = 2.0.4
PYTHON_CAN_ISOTP_SOURCE = can-isotp-$(PYTHON_CAN_ISOTP_VERSION).tar.gz
PYTHON_CAN_ISOTP_SITE = https://files.pythonhosted.org/packages/12/0b/35c6d46b066eb8d6fb914ed5a5ea14a800b8d2abb3ef2771fe05b4080c9b
PYTHON_CAN_ISOTP_LICENSE = MIT
PYTHON_CAN_ISOTP_LICENSE_FILES = LICENSE.txt
PYTHON_CAN_ISOTP_CPE_ID_VENDOR = can_isotp_project
PYTHON_CAN_ISOTP_CPE_ID_PRODUCT = can_isotp
PYTHON_CAN_ISOTP_SETUP_TYPE = setuptools

$(eval $(python-package))
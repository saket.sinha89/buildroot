################################################################################
#
# python-doipclient
#
################################################################################

PYTHON_DOIPCLIENT_VERSION = 1.1.1
PYTHON_DOIPCLIENT_SOURCE = doipclient-$(PYTHON_DOIPCLIENT_VERSION).tar.gz
PYTHON_DOIPCLIENT_SITE = https://files.pythonhosted.org/packages/9d/22/59fdfc934c4117cd7b9e2341ca629b4a5614f1f3f564c128630fff350290
PYTHON_DOIPCLIENT_LICENSE = MIT
PYTHON_DOIPCLIENT_LICENSE_FILES = LICENSE
PYTHON_DOIPCLIENT_CPE_ID_VENDOR = doipclient_project
PYTHON_DOIPCLIENT_CPE_ID_PRODUCT = doipclient
PYTHON_DOIPCLIENT_SETUP_TYPE = setuptools

$(eval $(python-package))